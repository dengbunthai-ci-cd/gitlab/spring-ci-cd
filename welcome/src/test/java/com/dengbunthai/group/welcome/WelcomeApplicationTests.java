package com.dengbunthai.group.welcome;

import com.dengbunthai.group.welcome.service.LogicCalculationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class WelcomeApplicationTests {

    @Autowired
    LogicCalculationService service;

    @Test
    void contextLoads() {
        int total = service.sum(1, 2, 3, 4, 5);
        Assert.isTrue(total == 15, "Sum is expected to be 15");
    }

    @Test
    void contextLoads2() {
        int total = service.sum(33, 22);
        Assert.isTrue(total == 55, "Sum is expected to be 55");
    }

    @Test
    void contextLoad3() {
        int total = service.sum(5, 5);
        Assert.isTrue(total == 10, "Sum is expected to be 10");
    }


}
