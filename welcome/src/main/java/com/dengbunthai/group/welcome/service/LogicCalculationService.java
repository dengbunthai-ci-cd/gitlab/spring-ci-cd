package com.dengbunthai.group.welcome.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LogicCalculationService {
    public int sum(int... nums) {
        return Arrays.stream(nums).sum();
    }
}
