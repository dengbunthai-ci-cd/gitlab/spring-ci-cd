package com.dengbunthai.group.welcome.api;

import com.dengbunthai.group.welcome.service.LogicCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api")
public class HelloRestController {

    @Autowired
    LogicCalculationService service;

    @GetMapping("hello")
    public ResponseEntity<Object> hello() {
        int total = service.sum(1, 2, 3, 4, 5);
        return ResponseEntity.status(HttpStatus.OK).body(
            new HashMap<String, Object>() {{
                put("total2", total);
            }}
        );
    }

}
